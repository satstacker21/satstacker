#!/usr/bin/env python

# satstacker

import json
import math
import os
import sys
import time

try:
    import requests
except:
    print("Looks like you are missing requests")
    print("this can be installed by running:")
    print("$ sudo pip install requests")
    sys.exit(1)

try:
    import cbpro
except:
    print("Looks like you are missing cbpro, the python client for Coinbase Pro")
    print("See https://github.com/danpaquin/coinbasepro-python for details")
    sys.exit(1)

debug = True
MissingKey = 'missing'

# config file should look like:
# {
#  "api_passphrase": "",
#  "api_key": "",
#  "iffft_trigger_key": "",
#  "api_secret": ""
# }

config_filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), 'config'))

if not os.path.exists(config_filepath):
	print('missing expected config file')
	sys.exit(1)

with open(config_filepath) as text_file:
	data = text_file.read()
	config = json.loads(data)

	key = config.get('api_key', MissingKey)
	passphrase = config.get('api_passphrase', MissingKey)
	secret = config.get('api_secret', MissingKey)
	ifttt_trigger_key = config.get('iffft_trigger_key', MissingKey)
	units = config.get('units', 'sats')

	if key == MissingKey or passphrase == MissingKey or secret == MissingKey:
		print('invalid config file')
		sys.exit(1)

cache_filepath = os.path.abspath(os.path.join(os.path.dirname(__file__), 'cache_data'))

bitcoin_product_id = 'BTC-USD'
minimum_time_between_purchases = 86400 * 2 - 1000	# just less than 2 days
maximum_time_between_purchases = 86400 * 5 - 1000	# just less than 5 days
max_time_to_wait_for_order = 60 * 60

# ratio of current price to all-time high price determines purchase amount
no_purchase_threshold = 0.97
medium_purchase_threshold = 0.90
large_purchase_threshold = 0.80
max_purchase_threshold = 0.70

minimum_usd_purchase = 10.0
medium_usd_purchase = 20.0
large_usd_purchase = 30.0
maximum_usd_purchase = 40.0

coinbase = cbpro.AuthenticatedClient(key, secret, passphrase)

def get_high_price():
	daily_candles = coinbase.get_product_historic_rates(bitcoin_product_id, granularity=86400)
	high_price = 0
	for candle in daily_candles:
		candle_high = candle[2]
		if candle_high > high_price:
			high_price = candle_high
	return high_price

def get_current_price():
	btc_ticker = coinbase.get_product_ticker(product_id=bitcoin_product_id)
	return float(btc_ticker["bid"])

def get_balance(currency):
	accts = coinbase.get_accounts()
	balance = 0
	for acct in accts:
		if acct["currency"] == currency:
			balance = acct["balance"]
			break
	return float(balance)

def last_purchase_date():
	date = 0
	if os.path.isfile(cache_filepath):
		with open(cache_filepath) as text_file:
			data = text_file.read()
		data_dict = json.loads(data)
		date = data_dict['purchase_date']
	return date

def get_purchase_amt():
	high_price = get_high_price()
	current_price = get_current_price()
	price_multiple = current_price / high_price
	print("price multiple: {0}".format(price_multiple))

	if price_multiple > no_purchase_threshold and time_since_last_purchase < maximum_time_between_purchases:
		print("no purchase, too close to all-time high")
		return 0.0

	usd_purchase_amt = minimum_usd_purchase
	if price_multiple < max_purchase_threshold:
		usd_purchase_amt = maximum_usd_purchase
	elif price_multiple < large_purchase_threshold:
		usd_purchase_amt = large_usd_purchase
	elif price_multiple < medium_purchase_threshold:
		usd_purchase_amt = medium_usd_purchase

	return math.floor(min(usd_purchase_amt, usd_balance) * 100) / 100 	# truncate to cents

def cache_data(purchase_date):
	data = {"purchase_date": purchase_date}
	with open(cache_filepath, 'w') as text_file:
		text_file.write(json.dumps(data))

def send_message(message):
	if debug:
		print(message)
	else:
		if ifttt_trigger_key == MissingKey:
			print("missing push key")
			return
		event_name = 'message'
		trigger_url = 'https://maker.ifttt.com/trigger/{0}/with/key/{1}'.format(event_name, ifttt_trigger_key) 
		r = requests.post(trigger_url, json={"value1": 'satstacker: {0}'.format(message)})
		if r.status_code != 200:
			print('failed: status code of IFTTT maker request was {0}'.format(r.status_code))

def send_purchase_notification(purchased_amt, purchase_price, rate):
	# Purchased 0.00005432 BTC for $10 at $59,885 / BTC.
	if units == 'BTC':
		message = 'Purchased {0} BTC for ${1} at ${2} / BTC.'.format(purchased_amt, int(purchase_price), "{:,}".format(int(rate)))
		send_message(message)
	else:
		message = 'Purchased {0} sats for ${1} at ${2} / BTC.'.format(int(purchased_amt * 100000000), int(purchase_price), "{:,}".format(int(rate)))
		send_message(message)


# begin

start_time = int(time.time())

usd_balance = get_balance("USD")
print('balance: {0}'.format(usd_balance))
if usd_balance < minimum_usd_purchase:
	rounded_balance = math.floor(usd_balance * 100) / 100
	send_message('Low balance at Coinbase: ${0}'.format(rounded_balance))
	sys.exit()

last_purchase_date = last_purchase_date()
time_since_last_purchase = start_time - last_purchase_date
print("last purchase was {0} hours ago".format(time_since_last_purchase / 3600))

# don't buy if it hasn't been long enough
if time_since_last_purchase < minimum_time_between_purchases:
	send_message('No purchase, hasn\'t been long enough')
	sys.exit()

usd_purchase_amt = get_purchase_amt()

if usd_purchase_amt == 0:
	send_message('No purchase, too close to all-time high')
	sys.exit()

current_price = get_current_price()

if debug:
	print('puchase amount: {0}'.format(usd_purchase_amt))
	send_purchase_notification(0.00016576, usd_purchase_amt, current_price)
else:
	order = coinbase.place_market_order(product_id=bitcoin_product_id, side='buy', funds=str(usd_purchase_amt))
	print(order)
	status = order['status']
	settled = order['settled']
	filled_size = float(order['filled_size'])

	if settled or status == 'pending':
		cache_data(start_time)

	print('status: {0}'.format(status))
	print('settled: {0}'.format(settled))
	if settled:
		send_purchase_notification(filled_size, usd_purchase_amt, current_price)
	elif status == 'pending':
		# the order was made but it wasn't immediately settled. periodically monitor the order until filled.
		order_id = order['id']
		time_since_order = 0
		while not settled and time_since_order < max_time_to_wait_for_order:
			print('not settled, let\'s wait...')
			time.sleep(60)
			order = coinbase.get_order(order_id)
			settled = order['settled']
			time_since_order = int(time.time()) - start_time

		if settled:
			filled_size = float(order['filled_size'])
			send_purchase_notification(filled_size, usd_purchase_amt, current_price)
		else:
			send_message('Timed out waiting for purchase order to settle')
	else:
		send_message('There was an error creating the purchase order')
